<?php

use PHPUnit\Framework\TestCase;

class CurrencyModelTest extends TestCase
{
    public function testObjectBeReturned()
    {
        $this->assertInstanceOf(\Af\CurrencyResponse::class, \Af\CurrencyModel::getByCode('RUB'));
    }
}