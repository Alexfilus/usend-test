<?php

namespace Af;

/**
 * Class Price
 * В реальности наверняка нужны будут дополнительные действия, и более сложный класс.
 */
class Price
{
    public static function format(float $value, CurrencyResponse $currency)
    {
        return number_format($value * $currency->getRate(), '2', '.', ' ') . ' ' . $currency->getTitle();
    }
}