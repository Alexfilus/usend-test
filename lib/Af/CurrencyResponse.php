<?php

namespace Af;

class CurrencyResponse
{
    /**
     * @var float $rate
     * @var string $title
     */
    private $rate;
    private $title;

    public function __construct(float $rate, string $title)
    {
        $this->rate = $rate;
        $this->title = $title;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}