<?php

namespace Af;

class DataResponseObject
{
    /**
     * @var string $name
     * @var string $text
     * @var string $price
     */
    private $name;
    private $text;
    private $price;

    /**
     * DataResponseObject constructor.
     * @param string $name
     * @param string $text
     * @param string $price
     */
    public function __construct(string $name, string $text, string $price)
    {
        $this->name = $name;
        $this->text = $text;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "<h1>{$this->name}</h1><p>{$this->text}</p><p>{$this->price}</p>";
    }
}