<?php

namespace Af;

/**
 * Class FileLogger
 * Предположительная реализация логгера, подходящая к заданию
 */
class FileLogger
{
    private $filePath;

    /**
     * FileLogger constructor.
     * @param string|null $filePath
     */
    public function __construct(?string $filePath = null)
    {
        $this->filePath = $_SERVER['DOCUMENT_ROOT'] . ($filePath ?? '/log.txt');
    }

    /**
     * @param string $msg
     */
    public function log(string $msg)
    {
        if (Config::getOption('IS_LOG_ACTIVE')) {
            file_put_contents($this->filePath, $msg . PHP_EOL, FILE_APPEND);
        }
    }
}