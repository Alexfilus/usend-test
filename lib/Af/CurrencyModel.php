<?php

namespace Af;

/**
 * Class CurrencyModel
 * Тут должна быть реализация получения реальных курсов валют из БД
 */
class CurrencyModel
{
    public static function getByCode(string $currencyCode)
    {
        switch ($currencyCode) {
            case 'RUB':
            default:
                return new CurrencyResponse(68, 'руб');
                break;
        }
    }
}