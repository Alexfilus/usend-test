<?php

namespace Af;

class RequestHandler
{
    const SOURCE_MYSQL = 'Mysql';
    const SOURCE_DEAMON = 'Daemon';
    /**
     * @var int $id
     * @var string $sourceType
     */
    private $id;
    private $sourceType;

    /**
     * RequestHandler constructor.
     * @throws \InvalidArgumentException
     */
    public function __construct()
    {
        if (!isset($_REQUEST['id']) || (int)$_REQUEST['id'] < 1) {
            throw new \InvalidArgumentException('Не задан ID');
        }
        $this->id = $_REQUEST['id'];

        if (!isset($_REQUEST['from'])) {
            throw new \InvalidArgumentException('Не задан параметр from');
        }
        switch ($_REQUEST['from']) {
            case self::SOURCE_MYSQL:
            case self::SOURCE_DEAMON:
                $this->sourceType = $_REQUEST['from'];
                break;
            default:
                throw new \InvalidArgumentException('Некорректный параметр from');
        }
    }

    /**
     * @return DataResponseObject
     */
    public function getData(): DataResponseObject
    {
        return $this->{$this->sourceType}();
    }

    /**
     * @return DataResponseObject
     */
    private function Mysql()
    {
        $logMsg = date('Y-m-d H:i:s'). " " . "getAdRecord(ID={$this->id})";
        $logger = new FileLogger();
        $logger->log($logMsg);
        $resp = getAdRecord($this->id);
        return new DataResponseObject(
            $resp['name'],
            $resp['text'],
            Price::format($resp['price'], CurrencyModel::getByCode(Config::getOption('CURRENCY')))
        );
    }

    /**
     * @return DataResponseObject
     */
    private function Daemon()
    {
        $logMsg = date('Y-m-d H:i:s'). " " . "get_deamon_ad_info(ID={$this->id})";
        $logger = new FileLogger();
        $logger->log($logMsg);
        [$idAds, $idComp, $idUser, $name, $text, $price] = explode("\t", get_deamon_ad_info($this->id));
        return new DataResponseObject(
            $name,
            $text,
            Price::format($price, CurrencyModel::getByCode(Config::getOption('CURRENCY')))
        );
    }
}