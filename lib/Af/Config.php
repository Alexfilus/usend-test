<?php

namespace Af;


class Config
{
    /**
     * @var self $instance
     * @var array $data
     */
    private static $instance;
    private static $data;

    /**
     * @return Config
     */
    public static function getInstance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Config constructor.
     */
    private function __construct()
    {
        $config = [];
        require_once($_SERVER['DOCUMENT_ROOT'] . '/config.php');
        self::$data = $config;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return self::$data;
    }

    public static function getOption(string $option)
    {
        $data = self::getInstance()->getData();
        return $data[$option];
    }
}